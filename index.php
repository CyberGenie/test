<?php
function __autoload($class_name)
{
    require_once $class_name . '.php';
}

$select = new Select(
    [
        'selectattribute' =>
        [
            'class' => 'classics',
            ''
        ],
        'optgroup' =>
        [
            'Машины',
            'Корабли',
            'Самолёты',
        ],
        'option' =>
        [
            ['label' => 'Корвет', 'value' => 'korvet', 'optgroup' => 'И то, и сё'],
            ['label' => 'ТУ', 'value' => 'tu'],
            ['label' => 'БМВ', 'value' => 'bmw5', 'optgroup' => 'Машины'],
            ['label' => 'Линкор', 'value' => 'linkor', 'optgroup' => 'Корабли'],
            ['label' => 'МиГ', 'value' => 'mig', 'optgroup' => 'Самолёты'],
            ['label' => 'Фрегат', 'value' => 'fregat', 'optgroup' => 'Корабли'],
            ['label' => 'Ауди', 'value' => 'audi', 'optgroup' => 'Машины'],
            ['label' => 'Су', 'value' => 'su', 'optgroup' => 'Самолёты'],
            ['label' => 'Мерседес', 'value' => 'mercedes', 'optgroup' => 'Машины'],
        ]
    ]
);

$select->editOption(['ТУ' => ['optgroup' => 'Самолёты']]);

$select->setOption([['label' => 'Танк', 'value' => 'tank', 'optgroup' => 'metal']]);

$select->setOptgroup(['metal']);

$select->editOption(['Корвет' => ['label' => 'БМП', 'value' => 'bmp', 'optgroup' => 'metal']]);

echo $select->getSelect();
