Ext.onReady(function(){
    Ext.define('People', {
        extend: 'Ext.data.Model',
        idProperty: 'peopleId',
        fields: ['age', 'man', 'woman']
    });
              
    var peopleStore = Ext.create('Ext.data.Store', {
        model: 'People',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: '',
            reader: {
                type: 'json',
                root: 'people'
            }
        }
    });

    Ext.define('Name', {
        extend: 'Ext.data.Model',
        idProperty: 'nameId',
        fields: ['name', 'count']
    });
              
    var nameStore = Ext.create('Ext.data.Store', {
        model: 'Name',
        autoLoad: false,
        proxy: {
            type: 'ajax',
            url: 'data/name.php',
            reader: {
                type: 'json',
                root: 'name'
            }
        }
    });

    var tree = Ext.create('Ext.tree.Panel', {
        title: 'Города',
        width: 250,
        region: 'west',
        listeners: {
            select: function(grid, city) {
                panel4.update(city.data.text);
                if ( city.raw.leaf ) {
                    peopleStore.proxy.url = 'data/' + city.data.id + '.json';
                    panel2.store.load();
                }
            }
        },
        root: {
            text: 'Страны СНГ',
            expanded: true,
            children:
            [{
                text: "Россия",
                children: [{
                    id: "mos",
                    text: "Москва",
                    leaf: true
                }, {
                    id: "spb",
                    text: "Санкт-Петербург",
                    leaf: true
                }, {
                    id: "vol",
                    text: "Волгоград",
                    leaf: true
                }],
                leaf: false,
                "expanded": true
            },
            {
                text: "Украина",
                leaf: false
            },
            {
                text: "Белоруссия"
            }]
        }
    });
    var panel2 = Ext.create('Ext.grid.Panel', {
        title: 'Демография',
        anchor: '100% 45%',
        store: peopleStore,
        columns: [
            { text: 'Возраст',  dataIndex: 'age', flex: 1 },
            { text: 'Мужчины', dataIndex: 'man', flex: 1 },
            { text: 'Женщины', dataIndex: 'woman', flex: 1 }
        ],
        listeners: {
            select: function(grid, record){
                panel3.store.load({
                    scope: this,
                    callback: (records) => {
                        panel4.update(city[0].data.text + stroka + '<br>' + records.length);
                    }
                });
                stroka = '<br>Возраст: ' + record.data.age + ', Мужчины: ' + record.data.man + ', Женщины: ' + record.data.woman;
                city = tree.getSelectionModel().getSelection();
            }
        }
    });
    var panel3 = Ext.create('Ext.grid.Panel', {
        title: 'Подробно',
        anchor: '100% 45%',
        store: nameStore,
        columns: [
            { text: 'Имя',  dataIndex: 'name', flex: 1 },
            { text: 'Количество', dataIndex: 'count', flex: 1 }
        ]
    });
    var panel4 = Ext.create('Ext.Panel', {
        border: 0,
        padding: 10
    });
    var center = Ext.create('Ext.Panel', {
        width: '100%',
        region: 'center',
        layout:'anchor',
        items: [
            panel2,
            panel3,
            panel4
        ]
    });
    
    var gridport = Ext.create('Ext.container.Viewport', {
        layout : 'border',
        items : [
            tree,
            center
        ]
    });
});