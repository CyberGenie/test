<?php

class Select implements SelectInterface
{
    /**
     * Массив значений тега <optgroup>
     *
     * @var array
     */
    protected $optgroup = [];

    /**
     * Массив значений тега <option>
     *
     * @var array
     */
    protected $option = [];

    /**
     * Массив значений атрибутов тега <select>
     *
     * @var array
     */
    protected $selectAttribute = [];

    /**
     * Конструктор класса с параметрами по умолчанию
     *
     * @param array $selectParam['optgroup'=>array, 'option'=>array, 'selectattribute'=>array]
     */
    public function __construct(array $selectParam = [])
    {
        if ((bool) $selectParam) {
            if (isset($selectParam['optgroup'])) {
                $this->setOptgroup($selectParam['optgroup']);
            }
            if (isset($selectParam['option'])) {
                $this->setOption($selectParam['option']);
            }
            if (isset($selectParam['selectattribute'])) {
                $this->setSelectAttribute($selectParam['selectattribute']);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function setOptgroup(array $optgroup)
    {
        $this->optgroup = array_merge($this->optgroup, $optgroup);
    }

    /**
     * @inheritDoc
     */
    public function removeOptgroup(array $optgroup)
    {
        if (!(bool) $optgroup) {
            $this->optgroup = [];
            return;
        }
        $this->optgroup = array_diff($this->optgroup, $optgroup);
    }

    /**
     * @inheritDoc
     */
    public function editOptgroup(array $optgroup)
    {
        foreach ($this->optgroup as $key => $oldVal) {
            foreach ($optgroup as $optKey => $newVal) {
                if ($oldVal == $optKey) {
                    $this->optgroup[$key] = $newVal;
                }
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function setOption(array $option)
    {
        $this->option = array_merge($this->option, $option);
    }

    /**
     * @inheritDoc
     */
    public function removeOption(array $option)
    {
        if (!(bool) $option) {
            $this->option = [];
            return;
        }
        foreach ($this->option as $key => $raw) {
            if (in_array($this->option[$key]['label'], $option)) {
                unset($this->option[$key]);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function editOption(array $option)
    {
        foreach ($this->option as $key => $raw) {
            foreach ($option as $editKey => $val) {
                if ($raw['label'] == $editKey) {
                    $this->option[$key] = array_merge($raw, $val);
                }
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function setSelectAttribute(array $selectAttribute)
    {
        $this->selectAttribute = array_merge($this->selectAttribute, $selectAttribute);
    }

    /**
     * @inheritDoc
     */
    public function removeSelectAttribute(array $selectAttribute)
    {
        $this->selectAttribute = array_diff_key($this->selectAttribute, array_flip($selectAttribute));
    }

    /**
     * @inheritDoc
     */
    public function getSelect()
    {
        $templateSelect = '<selectATTR>INNER</select>';
        $templateOptgroup = '<optgroup label="LABEL">OPTIONS</optgroup>';
        $templateOption = '<option value="VALUE">LABEL</option>';

        $selectAttribute = '';
        foreach ($this->selectAttribute as $key => $val) {
            if (!is_null($val)) {
                $selectAttribute .= ' ' . $key . '="' . $val . '"';
            } else {
                $selectAttribute .= ' ' . $key;
            }
        }

        $optgroup = '';
        if ((bool) $this->optgroup) {
            foreach ($this->optgroup as $label) {
                $optgroupOptions = '';
                foreach ($this->option as $option) {
                    if (isset($option['optgroup']) && !empty($option['optgroup']) && $option['optgroup'] == $label) {
                        $optgroupOptions .= str_replace(['VALUE', 'LABEL'], [$option['value'], $option['label']], $templateOption);
                    }
                }
                $optgroup .= str_replace(['LABEL', 'OPTIONS'], [$label, $optgroupOptions], $templateOptgroup);
            }
        }

        $options = '';
        if ((bool) $this->option) {
            foreach ($this->option as $option) {
                if (!isset($option['optgroup']) || empty($option['optgroup']) || !in_array($option['optgroup'], $this->optgroup)) {
                    $options .= str_replace(['VALUE', 'LABEL'], [$option['value'], $option['label']], $templateOption);
                }
            }
        }

        $fullSelect = str_replace(['ATTR', 'INNER'], [$selectAttribute, $optgroup . $options], $templateSelect);

        return $fullSelect;
    }
}
