<?php

interface SelectInterface
{
    /**
     * Устанавливает теги <optgroup>
     * 
     * @param array $optgroup[label_value:string]
     * @return void
     */
    public function setOptgroup(array $optgroup);

    /**
     * Удаляет указанные теги <optgroup>
     * 
     * @param array $optgroup[label_value:string]
     * @return void
     */
    public function removeOptgroup(array $optgroup);

    /**
     * Редактирует теги <optgroup>
     * 
     * @param array $optgroup[label_old_value:string=>label_new_value:string]
     * @return void
     */
    public function editOptgroup(array $optgroup);

    /**
     * Устанавливает теги <option>
     * 
     * @param array $option[]['label'=>string, 'value'=>string, ~'optgroup'=>string]
     * @return void
     */
    public function setOption(array $option);

    /**
     * Удаляет указанные теги <option>
     * 
     * @param array $option[label:string]
     * @return void
     */
    public function removeOption(array $option);

    /**
     * Редактирует параметры тега <option>
     * 
     * @param array $option[label_editable_tag:string=>[param=>param_new_value]]
     * @return void
     */
    public function editOption(array $option);

    /**
     * Устанавливает атрибуты тега <select>
     *
     * @param array $attribute[attribute_name:string=>attribute_value:string|NULL]
     * @return void
     */
    public function setSelectAttribute(array $selectAttribute);

    /**
     * Удаляет атрибуты тега <select>
     *
     * @param array $attribute[attribute_name:string]
     * @return void
     */
    public function removeSelectAttribute(array $selectAttribute);

    /**
     * Выдаёт готовый тег <select>
     *
     * @param void
     * @return string
     */
    public function getSelect();
}
